import React from "react";

import "./App.css";
import CarForm from "./Forms/CarForm";

function App() {
  return (
    <div className="App-header">
      <CarForm className="App-header" />
    </div>
  );
}

export default App;
