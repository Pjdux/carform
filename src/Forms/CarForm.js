import React, { Component, useState } from "react";
import { useForm } from "react-hook-form";
import Select from "react-select";
import FormWrap from "../styled-components/FormWrap";
const initialFormData = Object.freeze({
  FirstName: "",
  LastName: "",
});
const TestForm2 = () => {
  const [formData, updateFormData] = useState(initialFormData);

  const handleChange = (e) => {
    updateFormData({
      ...formData,

      [e.target.name]: e.target.value.trim(),
    });
  };

  const handleChange2 = (selectedOptions) => {
    updateFormData({ ...formData, selectedOptions });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);
    // ... submit to API or something
  };

  const options = [
    { value: "2019", label: "2019" },
    { value: "2020", label: "2020" },
  ];

  const makerOptions = [
    { BMW: "BMW", label: "BMW 2 Series" },
    { BMW: "BMW", label: "BMW 3 series" },
    { Toyota: "Toyota", label: "Toyota Camry" },
    { ToyotaNew: "ToyotaNew", label: "Toyota Yaris" },
    { Toyota: "Toyota", label: "Toyota Corolla" },
    { Acura: "Acura", label: "Acura TLX" },
    { Acura: "Acura", label: "Acura RDX" },
  ];

  const filteredOptions = makerOptions.filter(
    (x) => x.ToyotaNew === "ToyotaNew" || x.Acura === "Acura"
  );

  const filteredOptions2 = makerOptions.filter(
    (x) => x.Toyota === "Toyota" || x.BMW === "BMW"
  );

  return (
    <FormWrap className="App-header" onSubmit={handleSubmit}>
      <div className="phone">
        <h3>A Little About Your Self</h3>{" "}
        <input
          type="text"
          placeholder="Last Name"
          name="last name"
          onChange={handleChange}
        />
        <input
          type="text"
          placeholder="Firs Name"
          name="first name"
          onChange={handleChange}
        />
        <div className="phone">
          {" "}
          <input type="text" placeholder="Phone Number" name="phone" />
        </div>
      </div>

      <Select
        name="form-field-name"
        options={options}
        onChange={handleChange2}
        style={{ width: "300px" }}
      />

      {formData.value && formData.value === "2020" ? (
        <Select
          name="form-field-name"
          onChange={handleChange2}
          options={filteredOptions}
        />
      ) : formData.value && formData.value === "2019" ? (
        <Select
          name="form-field-name"
          onChange={handleChange2}
          options={filteredOptions2}
        />
      ) : null}

      <input
        type="text"
        placeholder="Color"
        name="color"
        onChange={handleChange}
      />

      <input
        type="text"
        placeholder="Plate"
        name="plate"
        onChange={handleChange}
      />

      <input type="submit" />
    </FormWrap>
  );
};

export default TestForm2;
